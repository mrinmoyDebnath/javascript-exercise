function sortCarModels(inventory){
    let models = [];
    if(inventory){
        let n = inventory.length;
        for(let i = 0; i < n; i++){
            models.push(inventory[i]['car_model'])
        }
    }
    models.sort()
    return models;
}

module.exports = sortCarModels;