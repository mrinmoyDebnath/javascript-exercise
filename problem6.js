function getSpecificCarsOnly(inventory=[], requiredCars=[]){
    let cars = [];
    if(inventory){
        let n =inventory.length;
        for(let i = 0; i < n; i++){
            
            if(requiredCars.includes(inventory[i]['car_make'])){
                cars.push(inventory[i]);
            }
        }
    }
    return cars;
}

module.exports = getSpecificCarsOnly;