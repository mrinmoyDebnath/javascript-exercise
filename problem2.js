function getLastCar(inventory){
    if(!inventory || inventory.length == 0){
        return []
    }
    const last = inventory.length - 1
    return inventory[last]
}

module.exports = getLastCar;