function getCarMakeYear(inventory){
    let carYears = []
    if(inventory){
        let n = inventory.length;
        for(let i = 0; i < n; i++){
            carYears.push(inventory[i]['car_year']);
        }
    }
    return carYears;
}

module.exports = getCarMakeYear;