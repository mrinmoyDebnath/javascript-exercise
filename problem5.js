const getCarMakeYear = require('./problem4.js');
function getOlderCars(inventory, year){
    let olderCars = []

    if(inventory){
        const n = inventory.length;
        const allYears = getCarMakeYear(inventory);
        for(let i = 0; i < n; i++){
            let carYear = allYears[i];
            if( carYear > year){
                olderCars.push(inventory[i]);
            }
        }
    }
    return olderCars;
}

module.exports = getOlderCars;